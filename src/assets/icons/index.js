import Vue from 'vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faUserSecret,
  faQuestionCircle,
  faSquare,
  faCheckSquare,
  faCode,
  faTerminal,
  faHeart,
  faMeteor,
  faArrowRight,
  faKey
} from '@fortawesome/free-solid-svg-icons'
import { faEye, faEyeSlash, faHeart as fasHeart } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

// You can add your icons directly in this plugin. See other examples for how you
// can add other styles or just individual icons.
library.add(
  faUserSecret,
  faQuestionCircle,
  faSquare,
  faCheckSquare,
  faCode,
  faTerminal,
  faHeart,
  fasHeart,
  faMeteor,
  faArrowRight,
  faEye,
  faEyeSlash,
  faKey
)

// Register the component globally
Vue.component('icon', FontAwesomeIcon)
