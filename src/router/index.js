import Vue from 'vue'
import VueRouter from 'vue-router'
import store from '@/store'
import SidebarLayout from '@/layouts/SidebarLayout'
import Sidebar from '@/components/shared/sidebar'
import AuthContent from '@/components/auth/auth-content'
import HomeContent from '@/components/home/home-content'
import UserMenu from '@/components/home/home-sidebar/user-menu'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Layout',
    component: SidebarLayout,
    redirect: { name: 'signin' },
    children: [
      {
        path: '/signin',
        name: 'signin',
        props: { sidebar: { logo: 'lg', content: 'signin-form', links: ['signup', 'recover'] } },
        components: {
          sidebar: Sidebar,
          content: AuthContent
        }
      },
      {
        path: '/signup',
        name: 'signup',
        props: { sidebar: { logo: 'lg', content: 'signup-form', links: ['signin', 'recover'] } },
        components: {
          sidebar: Sidebar,
          content: AuthContent
        }
      },
      {
        path: '/recover',
        name: 'recover',
        props: { sidebar: { logo: 'lg', content: 'recover-form', links: ['signin', 'signup'] } },
        components: {
          sidebar: Sidebar,
          content: AuthContent
        }
      },
      {
        path: '/home',
        meta: { private: true },
        props: { sidebar: { logo: 'sm', content: 'user-actions', links: ['signout'] } },
        components: {
          sidebar: Sidebar,
          content: HomeContent
        },
        children: [
          {
            path: '',
            name: 'home',
            component: UserMenu
          },
          {
            path: 'change-username',
            name: 'change-username',
            component: UserMenu
          },
          {
            path: 'change-email',
            name: 'change-email',
            component: UserMenu
          },
          {
            path: 'change-password',
            name: 'change-password',
            component: UserMenu
          }
        ]
      }
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

router.beforeEach((to, from, next) => {
  console.log('store.getters.isLoggedIn', store.getters.isLoggedIn)
  if ((to.meta.private || to.matched.some(({ meta }) => meta.private)) && !store.getters.isLoggedIn) {
    next({ name: 'signin' })
  } else {
    next()
  }
})

export default router
