import api from '@/store/services'
import router from '@/router'
import jwt from 'jsonwebtoken'

const state = {
  isLoggedIn: false,
  user: {},
  token: null,
  step: 'recover',
  loaders: {
    signin: false,
    signup: false,
    recover: false
  }
}

const getters = {
  isLoggedIn: (state) => state.isLoggedIn,
  isSigningIn: (state) => state.loaders.signin,
  isSigningUp: (state) => state.loaders.signup,
  isRecovering: (state) => state.loaders.recover,
  isAdmin: (state) => state.user.admin,
  username: (state) => state.user.username,
  createdAt: (state) => state.user.createdAt,
  email: (state) => state.user.email,
  step: (state) => state.step
}

const actions = {
  async signin({ commit }, { credentials, storeSession }) {
    try {
      commit('SET_LOADING', { loader: 'signin' })
      const { data } = await api.auth.post('signin', credentials)
      const { user, token } = data
      commit('SET_USER', { user, token, storeSession })
      router.push({ name: 'home' })
    } catch (error) {
      console.error(error)
    } finally {
      commit('SET_LOADING', { loader: 'signin', status: false })
    }
  },
  async signup({ commit }, { form }) {
    try {
      commit('SET_LOADING', { loader: 'signup' })
      const { data } = await api.auth.post('signup', form)
      const { user, token } = data
      commit('SET_USER', { user, token, storeSession: true })
      router.push({ name: 'home' })
    } catch (error) {
      console.error(error)
    } finally {
      commit('SET_LOADING', { loader: 'signup', status: false })
    }
  },
  async recover({ commit, state }, { form }) {
    try {
      const { email, ...rest } = form
      commit('SET_LOADING', { loader: 'recover' })
      const body = state.step === 'recover' ? { email } : { email, ...rest }
      await api.auth.post(state.step, body)
      commit('TOGGLE_STEP')
    } catch (error) {
      console.error(error)
    } finally {
      commit('SET_LOADING', { loader: 'recover', status: false })
    }
  }
}

const mutations = {
  SET_USER(state, { user, token, storeSession }) {
    state.isLoggedIn = true
    state.user = user
    state.token = token
    api.setToken(token)
    if (!storeSession) return
    const sessionData = jwt.sign({ user, token }, process.env.VUE_APP_JWT_SECRET)
    localStorage.setItem('session', sessionData)
  },
  TOGGLE_STEP(state) {
    state.step = state.step === 'recover' ? 'reset' : 'recover'
  },
  SET_LOADING(state, { loader, status = true }) {
    state.loaders[loader] = status
  }
}

export default {
  state,
  getters,
  actions,
  mutations
}
