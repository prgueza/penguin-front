import axios from 'axios'
import { merge } from 'lodash'

// eslint-disable-next-line
const URL_REGEX = process.env.NODE_ENV === 'production'
    ? /[(http(s)?)://(www.)?a-zA-Z0-9@:%._+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_+.~#?&//=]*)/gi
    : /localhost/

const API = {
  services: new Set()
}

const _configuration = {
  baseURL: null,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
}

const isHandlerEnabled = (config = {}) => {
  return !Object.prototype.hasOwnProperty.call(config, 'handlerEnabled') || config.handlerEnabled
}

const successHandler = (response) => {
  if (!isHandlerEnabled(response.config)) return response
  // responseHandler(response.config, response.status)
  return response
}

const errorHandler = (error) => {
  if (!isHandlerEnabled(error.config) || !error.response) return Promise.reject(error)
  // responseHandler(error.response.config, error.response.status, 'error')
  return Promise.reject(error)
}

// const responseHandler = ({ notify }, status = 500, type = 'success') => {
//   // ... Handle Notifications
// }

/* PUBLIC METHODS */
API.init = function(baseURL, configuration = {}) {
  if (!URL_REGEX.test(baseURL)) throw new Error(`Invalid URL: ${baseURL}`)
  merge(_configuration, configuration)
  _configuration.baseURL =
    baseURL.charAt(baseURL.length - 1) !== '/' ? baseURL : baseURL.substring(0, baseURL.length - 1)
}

API.use = function(name, { endpoint, ...config } = {}) {
  const baseURL = `${config.baseURL || _configuration.baseURL}${endpoint || `/${name}`}`
  const service = axios.create({ ...merge(config, _configuration), baseURL })
  service.interceptors.response.use(successHandler, errorHandler)
  API.services.add(name)
  API[name] = service
}

API.setToken = function(token, name) {
  if (name) {
    API[name].defaults.headers.Authorization = `Bearer ${token}`
  } else {
    API.services.forEach((service) => (API[service].defaults.headers.Authorization = `Bearer ${token}`))
  }
}

export default API
