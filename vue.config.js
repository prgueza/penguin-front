process.env.VUE_APP_VERSION = require('./package.json').version

module.exports = {
  chainWebpack: (config) => {
    config.plugin('html').tap((args) => {
      args[0].title = 'Penguin'
      return args
    })
  }
}
